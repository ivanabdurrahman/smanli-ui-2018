from django.apps import AppConfig


class ComingSoonConfig(AppConfig):
    name = 'COMING_SOON'
