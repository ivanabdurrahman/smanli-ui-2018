from django.shortcuts import render

# Create your views here.
response = {}
def index(request):
    response['author'] = "Ivan Abdurrahman"
    html = 'COMING_SOON/coming_soon.html'
    return render(request, html, response)
