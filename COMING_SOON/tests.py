from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import index

class ComingSoonUnitTest(TestCase):
	def test_url_is_exist(self):
		response = Client().get('/coming-soon/')
		self.assertEqual(response.status_code, 200)
		
	def test_root_url_now_is_using_index_page_from_coming_soon(self):
		found = resolve('/coming-soon/')
		self.assertEqual(found.func, index)
