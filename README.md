# Cool Website for ILUNI SMANLI-UI

Project ini dibuat dalam rangka memperkenalkan ILUNI SMANLI-UI sebagai ikatan alumni smanli, yang berusaha menyediakan suatu wadah untuk diakses publik tentang informasi-informasi umum yang berguna untuk adik-adik SMA khususnya SMAN 5 Depok yang ingin melanjutkan jenjang pendidikannya ke UI.

## Authors

* **Ivan Abdurrahman** - *Initial work* - [ivanabdu](https://www.behance.net/ivanabdu)

See also the list of [contributors](https://github.com/your/project/contributors) who participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Hat tip to anyone who's code was used
* Inspiration
* etc
